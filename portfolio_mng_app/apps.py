from __future__ import unicode_literals

from django.apps import AppConfig


class PortfolioMngAppConfig(AppConfig):
    name = 'portfolio_mng_app'
