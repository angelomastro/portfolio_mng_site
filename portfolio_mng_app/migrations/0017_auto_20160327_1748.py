# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-27 16:48
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio_mng_app', '0016_auto_20160327_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 27, 16, 48, 36, 714017, tzinfo=utc), editable=False),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='last_update_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 27, 16, 48, 36, 714074, tzinfo=utc)),
        ),
    ]
